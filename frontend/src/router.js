import { createWebHistory, createRouter } from "vue-router";
// импорт компонентов
import ListCards from "@/components/cart/ListCards";
import AddCard from "@/components/cart/AddCard";

// определяем маршруты
const routes = [
    {
        path: "/listCards",
        name: "list-cards",
        component: ListCards,
        meta: {
            title: "Список карточек"
        }
    },
    {
        path: "/addCard",
        name: "add-card",
        component: AddCard,
        meta: {
            title: "Добавление карточки"
        }
    }
];

const router = createRouter({
    history: createWebHistory(), // указываем, что будет создаваться история посещений веб-страниц
    routes // подключаем маршрутизацию
});

// указание заголовка компонентам (тега title), заголовки определены в meta
router.beforeEach(async (to, from, next) => {
    // для тех маршрутов, для которых не определены компоненты, подключается только App.vue
    // поэтому устанавливаем заголовком по умолчанию название "Главная страница"
    document.title = to.meta.title || 'Главная страница';
    next();
});

export default router;