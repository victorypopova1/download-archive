var express = require('express');
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

var db = require('./app/config/db.config.js'); // подключение настроек базы данных

db.sequelize.sync({force: false});

// Указание, что каталог files используется для хранения статических файлов
app.use(express.static("files"));

app.listen(3000);

var cors = require('cors');
var corsOptions = {
    origin: ['http://localhost:4200', 'http://localhost:4201'], // указываем, откуда будут приходить запросы
    credentials: true, // разрешаем обрабатывать запросы
    optionSuccessStatus: 200 // при успешной обработке запроса будет возвращён статус 200
};
app.use(cors(corsOptions));

var card = require('./app/route/card.route.js');
card(app);

