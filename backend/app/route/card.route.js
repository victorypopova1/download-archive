module.exports = (app) => {

    const card = require('../controller/card.controller');

    app.get('/api/cards', card.findAll);

    app.post('/api/addCard', card.create);

    app.post('/api/deleteCard/:id', card.delete);

};