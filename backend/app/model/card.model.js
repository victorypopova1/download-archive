module.exports = (sequelize, Sequelize) => {
    var Card = sequelize.define(
        'card', // определяем имя таблицы
        {
            id: {
                type: Sequelize.INTEGER,
                autoIncrement: true,
                primaryKey: true,
                allowNull: false
            },
            name: {
                type: Sequelize.STRING(100),
                allowNull: false
            },
            link: {
                type: Sequelize.STRING(200),
                allowNull: false
            },
            mime_type: {
                type: Sequelize.STRING(25),
                allowNull: false
            }
        });
    return Card;
};