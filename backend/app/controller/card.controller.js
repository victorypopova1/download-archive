var db = require('../config/db.config.js');
var globalFunctions = require('../config/global.functions.js');
var sequelize = db.sequelize;
var multiparty = require('multiparty');
var fs = require('fs');
var uuid = require('uuid');

var Card = db.card;

exports.findAll = (req, res) => {
    Card.findAll()
        .then(objects => {
            globalFunctions.sendResult(res, objects);
        })
        .catch(err => {
            globalFunctions.sendError(res, err);
        })
};


exports.create = async (req, res) => {
    // создаём объект для чтения данных, переданных со стороны клиента (передавали объект FormData)
    var form = new multiparty.Form();

    // читаем данные
    await form.parse(req, async (err, fields, files) => {
        if (!err) {
            var mimeType = files.file[0].headers['content-type']; // тип файла указывается так: image/png
            expansion = mimeType.split('/')[1]; // из "image/png" нужно извлечь только расширение

            var newName = uuid.v4() + "." + expansion; // вызываем функцию v4() для того, чтобы уникальный идентификатор был сгенерирован случайным образом
            var link = './files/' + newName;

            fs.copyFile(files.file[0].path, link, (err) => {
                if (err) {
                    throw err;
                }
                fs.unlink(files.file[0].path, (err) => {
                    if (err) {
                        console.error('Ошибка удаления файла:', err);
                    }
                });
            });;

            var name = fields.name[0];

            Card.create({
                name: name,
                link: newName,
                mime_type: mimeType
            }).then(object => {
                globalFunctions.sendResult(res, object);
            }).catch(err => {
                globalFunctions.sendError(res, err);
            })
        }
        else{
            globalFunctions.sendError(res, err);
        }
    });
};

exports.delete = (req, res) => {
    Card.findByPk(req.params.id)
    .then(async (object) => {
        // удаляем файл
        await fs.unlinkSync("files/" + object.dataValues.link);
        await Card.destroy({
            where: {
                id: req.params.id
            }
        }).then(() => {
            globalFunctions.sendResult(res, 'Запись удалена');
        }).catch(err => {
            globalFunctions.sendError(res, err);
        });
    }).catch(err => {
        globalFunctions.sendError(res, err);
    });
};

